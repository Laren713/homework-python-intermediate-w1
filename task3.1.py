#find grate common divisor
def gcd(a, b):
    if a == 0:
        return b
    return gcd(b % a, a)


class Fraction:

    def __init__(self, num, den):
        self.__num = num // gcd(num, den)
        self.__den = den // gcd(num, den)
        if not isinstance(num, int):
            raise TypeError('Numerator must be an integer')
        elif not isinstance(den, int):
            raise TypeError('Denominator must be an integer')
        elif den == 0:
            raise ZeroDivisionError('Cannot be divided by 0')

#access to attributes
    @property
    def num(self):
        return self.__num

    @property
    def den(self):
        return self.__den

    @num.setter
    def num(self, num):
        self.__num = num

    @den.setter
    def den(self, den):
        self.__den = den

#rewritten methods
    def __add__(self, new_fraction):
        return Fraction(self.num * new_fraction.den + self.den * new_fraction.num, self.den * new_fraction.den)

    def __sub__(self, new_fraction):
        return Fraction(self.num * new_fraction.den - self.den * new_fraction.num, self.den * new_fraction.den)

    def __mul__(self, new_fraction):
        return Fraction(self.num * new_fraction.num, self.den * new_fraction.den)

    def __truediv__(self, new_fraction):
        return Fraction(self.num * new_fraction.den, self.den * new_fraction.num)

    def __str__(self):
        return f'{self.num}/{self.den}'

#static methods
    @staticmethod
    def add(fract1, fract2):
        new_num = fract1.num * fract2.den + fract1.den * fract2.num
        new_den = fract2.den * fract1.den
        res = gcd(new_num, new_den)
        return f'{new_num//res}/{new_den//res}'

    @staticmethod
    def sub(fract1, fract2):
        new_num = fract1.num * fract2.den - fract1.den * fract2.num
        new_den = fract2.den * fract1.den
        res = gcd(new_num, new_den)
        return f'{new_num // res}/{new_den // res}'

    @staticmethod
    def mul(fract1, fract2):
        new_num = fract1.num * fract2.num
        new_den = fract1.den * fract2.den
        res = gcd(new_num, new_den)
        return f'{new_num // res}/{new_den // res}'

    @staticmethod
    def truediv(fract1, fract2):
        new_num = fract1.num * fract2.den
        new_den = fract1.den * fract2.num
        res = gcd(new_num, new_den)
        return f'{new_num // res}/{new_den // res}'

#class method
    @classmethod
    def string_to_fraction(cls, string):
        values = [int(i) for i in string.split('/')]
        assert len(values) == 2
        return cls(*values)


frac1 = Fraction(1,2)
frac2 = Fraction(2,3)
test1 = Fraction.string_to_fraction('7/13')
test2 = Fraction.add(frac1, frac2)
test3 = Fraction.sub(frac1, frac2)
test4 = Fraction.mul(frac1, frac2)
test5 = Fraction.truediv(frac1, frac2)
print(f'Transform string to fraction: {test1}')
print(f'Addition of fractions {frac1} and {frac2}: {test2}\n'
      f'Subtraction of fractions {frac1} and {frac2}: {test3}\n'
      f'Multiplication of fractions {frac1} and {frac2}: {test4}\n'
      f'Division of fractions {frac1} and {frac2}: {test5}')