from math import sqrt


class Point2D:

    _count_obj = 0

    def __init__(self, x, y):
        self.x = x
        self.y = y
        if isinstance(x, str) or isinstance(x, bool):
            raise TypeError('X must be a number')
        elif isinstance(y, str) or isinstance(y, bool):
            raise TypeError('Y must be a number')
        Point2D._count_obj += 1

    def distance(self, point):
        return f'Distance between two points in a plane: {round(sqrt((self.x - point.x)**2 + (self.y - point.y)**2), 2)}'

    def get_count(self):
        return f'Number of objects: {self._count_obj}'


class Point3D(Point2D):

    def __init__(self, x, y, z):
        super().__init__(x, y)
        self.z = z

    def distance(self, point):
        return f'Distance between two points in space: ' \
               f'{round(sqrt((self.x - point.x)**2 + (self.y - point.y)**2 + (self.z - point.z)**2), 2)}'


p1 = Point2D(6,9)
print(p1.get_count())
p2 = Point2D(13,7)
print(p1.distance(p2))
print(p2.get_count())
p3 = Point3D(6,9,13)
print(p3.get_count())
p4 = Point3D(13,7,6)
print(p3.get_count())
print(p3.distance(p4))